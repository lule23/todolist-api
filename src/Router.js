import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import TodosPage from "./pages/Todos";
import TodoPage from "./pages/Todo";

const Router = () => {
    return (
        <div>
            <header>Application header</header>
            <BrowserRouter>
                <Switch>
                    <Route exact={true} path="/" component={() => <Redirect to="/todos" />} />
                    <Route  path="/todos" component={TodosPage} />
                    <Route  path="/todo/:todoID" component={TodoPage} />
                    <Route component={() => <Redirect to="/todos" />} />
                </Switch>
            </BrowserRouter>
            <footer>Application footer</footer>
        </div>

    )
}

export default Router;