import React from "react";
import { Link } from "react-router-dom";

const Todo = ({ title, completed, onDelete, onComplete, id, onLinkClick }) => {
    return (
        <div>
            <h4>{title}</h4>
            <input type="checkbox" checked={completed}  onChange={onComplete} />
            <button onClick={onDelete}>Delete</button>

            {/*<Link to={`/todo/${id}`}>More info...</Link>*/}
            <a href="#" onClick={onLinkClick}>Click here</a>
        </div>
    )
}

export default Todo;