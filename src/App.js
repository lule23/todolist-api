import React from "react"
import axios from "axios";
import Router from "./Router";

axios.defaults.baseURL = "https://jsonplaceholder.typicode.com"

class App extends React.Component {
  render() {
    return <Router />
  }
}

export default App;
