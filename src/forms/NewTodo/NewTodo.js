import React from "react";

class NewTodo extends React.Component {
    state = {
        title: "",
        completed: false
    }

    onChange = (event) => {
        const { value, name, checked } = event.target;
        this.setState({ [name]: name === "completed" ? checked : value });
    }



    render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={(event) => handleSubmit(event, this.state)}>
                <input type="text" name="title" value={this.state.title} onChange={this.onChange}/>
                <input type="checkbox" name="completed" onChange={this.onChange} checked={this.state.completed}/>
                <button type="submit">ADD</button>
            </form>
        )
    }
}

export default NewTodo;