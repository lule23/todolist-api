import React from "react";

const TodoPage = ({ location }) => {
    const activeTodo = location.state.todo;
    const { title, userId, completed, id } = activeTodo;

    return <div>
        <h1>{id} - {title}</h1>
        <h4>User that created this todo: {userId}</h4>
        <input type="checkbox" checked={completed}/>
    </div>
}

export default TodoPage;