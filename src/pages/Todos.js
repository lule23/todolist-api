import React, { useState, useEffect } from "react"
import axios from "axios";
import Todo from "../components/Todo/Todo";
import NewTodo from "../forms/NewTodo/NewTodo";

//  useState => daje funkcionalnost state-a
//  useEffect => daje funkcionalnost lifecycle metoda
//  useRef => referenca ka elementu/komponenti

const initialState = {
    userId: 1,
    todos: [],
    formVisible: false
}


const TodosPage = ({ history }) => {
    /*state = {
        userId: 1,
        todos: [],
        formVisible: false
    }*/

    //  prva vrednost je vrednos state-a, druga vrednost je funckcija za izmenu state-a
    const [state, setState] = useState(initialState);

    /*

    Može da se ima više steate-ova unutar jedna funkcionalne komponente

    const [todos, setTodos] = useState([]);
    const [formVisible, setFormVisible] = useState(false);

    */

    const toogleForm = () => {
        setState(prevState => ({ ...prevState, formVisible: !prevState.formVisible}));
    }

    /*async componentDidMount() {
        try {
            const response = await axios.get(`/todos?userId=${this.state.userId}`);
            this.setState({ todos: response.data  });
        } catch (e) {
            console.log(e);
        }
    }*/

    useEffect(function () {
        const fetchTodos = async  () => {
            try {
                const response = await axios.get(`/todos?userId=${state.userId}`);
                setState({ ...state, todos: response.data  });
            } catch (e) {
                console.log(e);
            }
        }

        fetchTodos();

    }, []);

    const addNewTodo = async (event, values) => {
        event.preventDefault();

        try  {
            const response = await axios.post("/todos", { userId: this.state, title: values.title, completed: values.completed });
            const oldTodos = state.todos;
            oldTodos.push(response.data);

            // this.setState({ todos: oldTodos  });
            setState({ ...state, todos: oldTodos  });

            /*

            const obj1 = { name: "nikola"};
            const obj2 = { age: 19 }
            const obj3 = { ...obj1, ...obj2 }

            */
        }   catch (e) {
            console.log(e)
        }
    }

    const deleteTodo = async (id) => {
        try {
            const response = await axios.delete(`/todos/${id}`);
            const oldTodos = state.todos;

            const todoToDeleteIndex = oldTodos.findIndex(todo => todo.id === id);
            oldTodos.splice(todoToDeleteIndex, 1);
            console.log(oldTodos)

            setState({ ...state, todos: oldTodos  });

        } catch(e) {
            console.log(e)
        }
    }

    const toogleComplete = async (id, value) => {
        try {
            const response = await axios.put(`/todos/${id}`, { completed: value });
            const oldTodos = state.todos;

            const todoToUpdateIndex = oldTodos.findIndex(todo => todo.id === id);
            oldTodos.splice(todoToUpdateIndex, 1, { ...oldTodos[todoToUpdateIndex], ...response.data } );

            setState({ ...state, todos: oldTodos  });

        } catch(e) {
            console.log(e)
        }
    }

    const redirect = (event, id, todo) => {
        event.preventDefault();
        return history.push(`/todo/${id}`, { todo });
    }

        //  funkcionalna komponenta nema render metodu, već odmah vraća JSX
        return (
            <div style={{ display: "flex", justifyContent: "center", flexDirection: "column", textAlign: "left"}}>
                {state.todos.map(todo => (
                    <Todo
                        key={todo.id + Math.random()}
                        title={todo.title}
                        completed={todo.completed}
                        onDelete={() => deleteTodo(todo.id)}
                        onComplete={(event) => toogleComplete(todo.id, event.target.checked)}
                        id={todo.id}
                        onLinkClick={(event) => redirect(event, todo.id, todo)}
                    />
                ))}
                <button onClick={toogleForm}>Add new todo</button>
                {state.formVisible && <NewTodo handleSubmit={addNewTodo}/>}
            </div>
        )
}

export default TodosPage;
